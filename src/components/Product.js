import React from 'react'
import PropTypes from 'prop-types'

const Product = ({ price, quantity, title }) => (
  <div className="col-sm-3 col-md-3">
    {/* {title} - &#36;{price}{quantity ? ` x ${quantity}` : null} */}
    <div className="thumbnail">
      <h3>{title}</h3>
      <img src="img/test.jpeg" alt="..." />
      <b>{price}-{quantity}</b>
      <button className="btn btn-success col-xs-12" >Add to cart</button>
      <div className="clearfix"></div>
    </div>
  </div>
)

Product.propTypes = {
  price: PropTypes.number,
  quantity: PropTypes.number,
  title: PropTypes.string
}

export default Product
