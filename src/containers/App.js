import React from 'react'
import ProductsContainer from './ProductsContainer'
import CartContainer from './CartContainer'
import './App.css';

const App = () => (
  <div className="container">
    <div className="row">
      <h2 className="animate-text">Welcome to shopping</h2>
      <ProductsContainer />
      <CartContainer />
    </div>
  </div>
)

export default App